package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
)

func spectatorTransmitor() {
	spectarClients := make(map[*websocket.Conn]bool)

	// handle connection events
	go func() {
		for {
			ws := <-spectatorConnectionEventChan

			fmt.Println("should be adding spectator to list")
			if !spectarClients[ws] {
				connectionEventCh <- ConnectionEvent{
					EventID:  ConnEventNewSpectator,
					SenderID: 0,
					ws:       ws,
				}
				spectarClients[ws] = true
				fmt.Println("spectator added to list:", ws.RemoteAddr())
			} else {
				ws.Close()
				delete(spectarClients, ws)
				fmt.Println("spectator removed from list:", ws.RemoteAddr())
			}
		}
	}()

	// handle game-messages
	for {
		event := <-toSpectators
		for ws := range spectarClients {
			if err := ws.WriteJSON(event); err != nil {
				spectatorConnectionEventChan <- ws
			}
		}
	}
}

func wsSpectator(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		panic(err)
	}
	spectatorConnectionEventChan <- ws
}
