package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/gorilla/websocket"
)

type session struct {
	// communication
	toEngine          chan Event
	toAdmin           chan Event
	toClients         chan Event
	connectToAdmin    chan *websocket.Conn
	connectionEventCh chan ConnectionEvent

	// configuration
	startMoney  int
	porto       int
	roundPrice  int
	roundReward int

	// state
	history        []Event
	mailbox        []Event
	players        map[playerIDType]*playerType
	waitingForMsgs int

	currentRound uint
	currentStage uint
	curStageTime time.Time

	// data
	dataDirName string
	dfile       *json.Encoder
}

type playerType struct {
	toPlayer        chan Event
	connectToPlayer chan *websocket.Conn

	ownQuestion     expertiseType
	ownExpertise    expertiseType
	knownQuestions  map[expertiseType]playerIDType
	knownExpertises map[expertiseType]playerIDType
	waitingForMsgs  bool
	haveWon         bool
}

func (S *session) eventListener() {
	for {
		select {
		case connEvent := <-S.connectionEventCh:
			S.handleConnectionEvent(connEvent)

		default:
			select {
			case connEvent := <-S.connectionEventCh:
				S.handleConnectionEvent(connEvent)

			case event := <-S.toEngine:
				// click-events are not sent trhough the game-event handler
				if event.EventID == EventClick {
					toClickWriter <- event
					continue
				}
				S.handleGameEvent(event)
				// after each event
				if S.waitingForMsgs == 0 {
					S.nextStage(false)
				}
				// if S.currentStage == 3 {
				//	// time.Sleep(1 * time.Second)
				//	S.nextRound(false)
				//}
			}
		}
	}
}

func (S *session) handleConnectionEvent(connEvent ConnectionEvent) {
	switch connEvent.EventID {

	case ConnEventNewAdmin:
		S.connectToAdmin <- connEvent.ws
		for _, event := range S.history {
			S.toAdmin <- event
		}

	case ConnEventAdminLost:
		fmt.Println("ConnEventAdminLost not yet implemented!")

	case ConnEventNewPlayer:
		player, ok := S.players[connEvent.SenderID]
		if ok {
			connEvent.ws.WriteJSON(connEvent.SenderID)
			player.connectToPlayer <- connEvent.ws
		} else {
			connEvent.ws.Close()
			return
		}
		var r, s uint = 0, 0
		for _, event := range S.history {
			// player "is sender" OR "is receiver AND previous stage"

			if event.SenderID == connEvent.SenderID ||
				event.ReceiverID == PlayerIDAll ||
				event.ReceiverID == connEvent.SenderID &&
					(r < S.currentRound || s < S.currentStage ||
						event.EventID == EventSuggestContact ||
						event.EventID == EventRemoveAllContactSuggestions) {

				player.toPlayer <- event

				if event.EventID == EventNextRound {
					r++
					s = 0
				}
				if event.EventID == EventNextStage {
					s++
				}
			}
		}

	case ConnEventPlayerLost:
		fmt.Println("ConnEventPlayerLost not yet implemented!")

	case ConnEventNewSpectator:
		for _, event := range S.history {
			connEvent.ws.WriteJSON(event)
		}
	}
}

func (S *session) handleGameEvent(event Event) {
	var sender = S.players[event.SenderID]
	var receiver = S.players[event.ReceiverID]

	event.Time = time.Now()
	event.Valid = false // until the opposite is proven

	switch event.EventID {

	case EventAddPlayer:
		if event.FromAdmin &&
			S.currentRound == 0 &&
			S.currentStage == 0 {

			newPlayerID := S.addPlayer()

			event.Valid = true
			event.SenderID = newPlayerID
			event.ReceiverID = PlayerIDAll
			event.Reward = S.startMoney

			for _, player := range S.players {
				player.toPlayer <- event
			}
		}

	case EventNextRound:
		// Check that the message is valid
		if event.FromAdmin {
			S.nextRound(true)
			return
		}

	case EventNextStage:
		// chech that the message is valid
		if event.FromAdmin {
			S.nextStage(true)
			return
		}

	case EventMsgInquiry:
		if S.currentStage == 1 && sender.waitingForMsgs {
			event.Valid = true
			event.Question = sender.ownQuestion
			event.Expertise = sender.ownExpertise
			event.Porto = S.porto
			if event.Expertise == receiver.ownQuestion && !receiver.haveWon {
				event.Reward = S.roundReward
				receiver.haveWon = true
			}
			S.mailbox = append(S.mailbox, event)
		}
		sender.toPlayer <- event

	case EventMsgReply:
		if S.currentStage == 2 && sender.waitingForMsgs {
			event.Valid = true
			event.Porto = S.porto
			event.Question = receiver.ownQuestion
			if referredID, ok := sender.knownExpertises[receiver.ownQuestion]; ok {
				event.Expertise = receiver.ownQuestion
				event.ReferredID = referredID
				if event.Expertise == receiver.ownQuestion && !receiver.haveWon {
					event.Reward = S.roundReward
					receiver.haveWon = true
				}
			}
			S.mailbox = append(S.mailbox, event)
		}
		sender.toPlayer <- event

	case EventNoMoreMsgs:
		if sender.waitingForMsgs &&
			time.Since(S.curStageTime) > 500*time.Millisecond {
			event.Valid = true
			sender.waitingForMsgs = false
			S.waitingForMsgs--
		}
		sender.toPlayer <- event

	case EventStartTimer:
		if event.FromAdmin && event.Porto == int(S.currentStage) && event.Reward > 0 {
			event.Valid = true
			for _, player := range S.players {
				player.toPlayer <- event
			}
		}
		S.toAdmin <- event
		return

	case EventShuffleContactLists, EventStartQuestionnaire:
		if event.FromAdmin {
			event.Valid = true
			event.ReceiverID = PlayerIDAll
			for _, player := range S.players {
				player.toPlayer <- event
			}
		}

	case EventSuggestContact, EventRemoveAllContactSuggestions:
		if event.FromAdmin {
			event.Valid = true
			if event.ReceiverID == PlayerIDAll {
				for _, player := range S.players {
					player.toPlayer <- event
				}
			} else {
				if receiver != nil {
					receiver.toPlayer <- event
				} else {
					fmt.Println("Error trying to suggest contacts:\n", event)
				}
			}
		}

	default:
		fmt.Println("Received event with unknown EventID:")
		fmt.Println(event)
	}

	// all incomming messages are put in history and sent to admin
	S.applyEvent(event)
}

func (S *session) addPlayer() playerIDType {
	var playerID = playerIDType(len(S.players) + 1)
	var player = playerType{
		toPlayer:        make(chan Event),
		connectToPlayer: make(chan *websocket.Conn),
		knownQuestions:  make(map[expertiseType]playerIDType),
		knownExpertises: make(map[expertiseType]playerIDType),
	}

	go wsClientWriter(
		playerID,
		S.toEngine,
		player.toPlayer,
		player.connectToPlayer,
	)
	S.players[playerID] = &player

	return playerID
}

func (S *session) nextRound(fromAdmin bool) {
	S.currentRound++
	S.currentStage = 0

	var shuffle = rand.Perm(len(S.players))
	var alphabetShuffle = rand.Perm(len(S.players))
	var q = 0
	for id, player := range S.players {
		// unlearn contact information
		player.knownQuestions = make(map[expertiseType]playerIDType)
		player.knownExpertises = make(map[expertiseType]playerIDType)
		player.haveWon = false

		// choose give question and expertise
		// Effectievly players are arranged in a ring with random order.
		// Each player is the expert for the "next" player in the ring.
		// On top the alphabet order is shuffled, so that having question A doesn't imply having expertise B.
		// This last step is purly aestetic,
		// as using the ordinary alphabeth order would not give players any extra information.
		player.ownQuestion = expertiseType(1 + alphabetShuffle[shuffle[q]])
		player.ownExpertise = expertiseType(1 + alphabetShuffle[(shuffle[q]+1)%len(S.players)])
		q++

		player.knownQuestions[player.ownQuestion] = id
		player.knownExpertises[player.ownExpertise] = id

		// transmit new round event
		var event = Event{
			EventID:    EventNextRound,
			ReceiverID: id,
			Question:   player.ownQuestion,
			Expertise:  player.ownExpertise,
			Reward:     S.roundPrice,
			FromAdmin:  fromAdmin,
			Valid:      true,
			Time:       time.Now(),
		}

		player.toPlayer <- event
		S.applyEvent(event)
	}
	S.nextStage(false)
}

func (S *session) nextStage(fromAdmin bool) {

	// empty mailbox
	for _, msg := range S.mailbox {
		var receiver = S.players[msg.ReceiverID]
		switch msg.EventID {

		case EventMsgInquiry:
			receiver.knownQuestions[msg.Question] = msg.SenderID
			receiver.knownExpertises[msg.Expertise] = msg.SenderID

		case EventMsgReply:
			receiver.knownExpertises[msg.Expertise] = msg.ReferredID

		default:
			fmt.Println("ERROR! unexpected message in mailbox:")
			fmt.Println(msg)
		}
		receiver.toPlayer <- msg
	}
	S.mailbox = S.mailbox[:0]

	// start next stage
	S.currentStage++
	S.curStageTime = time.Now()

	S.waitingForMsgs = len(S.players)
	var event = Event{
		EventID:    EventNextStage,
		ReceiverID: PlayerIDAll,
		FromAdmin:  fromAdmin,
		Valid:      true,
		Time:       S.curStageTime,
	}

	for _, player := range S.players {
		player.waitingForMsgs = true
		player.toPlayer <- event
	}
	S.applyEvent(event)
}

func (S *session) initDataFile(dataDirName string) {

	// Set file name and make sure old histories are not overwritten
	dFileName := dataDirName + "/gameEvents"
	_, err := os.Stat(dFileName + ".txt")
	for !os.IsNotExist(err) {
		dFileName += "I"
		_, err = os.Stat(dFileName + ".txt")
	}
	dFileName += ".txt"

	// create data-file
	if dfile, err := os.Create(dFileName); err != nil {
		panic(err)
	} else {
		S.dfile = json.NewEncoder(dfile)
	}
}

func (S *session) applyEvent(event Event) {
	S.history = append(S.history, event)
	S.dfile.Encode(event)
	S.toAdmin <- event
	toSpectators <- event
}

func (S *session) loadHistory(fname string) {
	file, err := os.Open("files/" + fname)
	defer file.Close()
	if err != nil {
		fmt.Println("Could not open history-file", fname, ":\n", err)
		return
	}

	scanner := bufio.NewScanner(file)
	var event Event
	for scanner.Scan() {
		if err = json.Unmarshal([]byte(scanner.Text()), &event); err != nil {
			fmt.Println("Error occured when reading history. Could not interpret line as JSON-event. :\n", err)
			fmt.Println("Rest of history not loadet!")
			return
		}

		switch event.EventID {
		case EventNextStage:
			S.nextStage(event.FromAdmin)

		case EventNextRound:
			id := event.ReceiverID
			player := S.players[id]
			if id == 1 {
				S.currentRound++
			}
			S.currentStage = 0

			// unlearn contact information
			player.knownQuestions = make(map[expertiseType]playerIDType)
			player.knownExpertises = make(map[expertiseType]playerIDType)
			player.haveWon = false

			// choose give question and expertise
			player.ownQuestion = expertiseType(event.Question)
			player.ownExpertise = expertiseType(event.Expertise)

			player.knownQuestions[player.ownQuestion] = id
			player.knownExpertises[player.ownExpertise] = id

			// transmit new round event
			var event = Event{
				EventID:    EventNextRound,
				ReceiverID: id,
				Question:   player.ownQuestion,
				Expertise:  player.ownExpertise,
				Reward:     S.roundPrice,
				FromAdmin:  event.FromAdmin,
				Valid:      true,
				Time:       time.Now(),
			}

			player.toPlayer <- event
			S.applyEvent(event)

		default:
			S.handleGameEvent(event)
		}
	}

	if err = scanner.Err(); err != nil {
		fmt.Println("Unexpected error from bufio.Scanner:\n", err)
	}

	fmt.Println("Finished loading history")
}
