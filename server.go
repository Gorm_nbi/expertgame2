package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{}

// S is the active instance of a game-session
var toEngine = make(chan Event)
var connectionEventCh = make(chan ConnectionEvent)

// Prepare spectator handleing
var toSpectators = make(chan Event)
var spectatorConnectionEventChan = make(chan *websocket.Conn)

func main() {
	// handle commandline flags:
	loadHistory := flag.String("load_history", "", "-load_history='filename'")
	flag.Parse()

	S := new(session)
	S.toEngine = toEngine
	S.connectionEventCh = connectionEventCh
	S.history = make([]Event, 0)
	S.mailbox = make([]Event, 0)
	S.players = make(map[playerIDType]*playerType)
	S.waitingForMsgs = -1

	// Game settings:
	S.startMoney = 100
	S.porto = 1
	S.roundPrice = 0
	S.roundReward = 10

	// create session-data directory
	// first select an unused name
	S.dataDirName = "files/data_" + time.Now().Local().Format("2006-01-02")
	_, err := os.Stat(S.dataDirName)
	for i, originalName := 2, S.dataDirName; !os.IsNotExist(err); i++ {
		S.dataDirName = fmt.Sprintf("%s_x%v", originalName, i)

		_, err = os.Stat(S.dataDirName)
	}
	// then actually create the directory
	if err := os.Mkdir(S.dataDirName, 0777); err != nil {
		panic(err)
	}

	S.initDataFile(S.dataDirName)

	// handleAdminCLients
	S.toAdmin = make(chan Event)
	S.connectToAdmin = make(chan *websocket.Conn)

	// start clientWriters
	go wsClientWriter(PlayerIDAdmin, S.toEngine, S.toAdmin, S.connectToAdmin)
	go spectatorTransmitor()

	if *loadHistory != "" {
		S.loadHistory(*loadHistory)
	}
	go S.eventListener()
	go clickWriter(S.dataDirName)
	go controlQuestionWriter(S.dataDirName)
	go questionnaireWriter(S.dataDirName)

	// Define possible requests
	http.Handle("/", http.FileServer(http.Dir("./files")))
	http.HandleFunc("/wsAdmin", wsAdmin)
	http.HandleFunc("/wsPlayer", wsPlayer)

	http.HandleFunc("/wsControlQuestions", wsControlQuestions)
	http.HandleFunc("/wsQuestionnaire", wsQuestionnaire)

	// handleing spectators
	http.HandleFunc("/wsSpectator", wsSpectator)

	// Listen and serve (until an error occours...)
	if err := http.ListenAndServeTLS(":8080", "cert.pem", "key.pem", nil); err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}

func wsAdmin(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		panic(err)
	}

	// validate password
	var password string
	if err := ws.ReadJSON(&password); err != nil || password != "fiskepind" {
		ws.Close()
		fmt.Println("Failed admin login")
		return
	}

	connectionEventCh <- ConnectionEvent{
		EventID:  ConnEventNewAdmin,
		SenderID: PlayerIDAdmin,
		ws:       ws,
	}
}

func wsPlayer(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("websocket upgrade error:", err)
		return
	}
	var requestedPlayerID int
	if err := ws.ReadJSON(&requestedPlayerID); err != nil {
		fmt.Println("Error reading requested playerID:", err)
		ws.Close()
		return
	}
	connectionEventCh <- ConnectionEvent{
		EventID:  ConnEventNewPlayer,
		SenderID: playerIDType(requestedPlayerID),
		ws:       ws,
	}
}

func wsClientWriter(
	clientID playerIDType,
	toEngine chan Event,
	toClient chan Event,
	newConnectionCh chan *websocket.Conn,

) {
	var ws *websocket.Conn
	for {
		select {
		case newWS := <-newConnectionCh:
			if ws != nil {
				ws.Close()
			}
			ws = newWS
			go wsClientReader(clientID, toEngine, ws)
			fmt.Println("Client", clientID, "connected to client", ws.RemoteAddr())

		case event := <-toClient:

			if ws != nil {
				if err := ws.WriteJSON(event); err != nil {
					ws.Close()
					ws = nil
					fmt.Println("Error transmitting to player", clientID, ":", err)
					fmt.Println("  connection closed!")
				}
			}
		}
	}
}

func wsClientReader(
	clientID playerIDType,
	toEngine chan Event,
	ws *websocket.Conn,

) {
	for {
		var event Event
		if err := ws.ReadJSON(&event); err != nil {
			fmt.Println("Client", clientID, "connection closed. Error:", err)
			break
		}

		if clientID == PlayerIDAdmin {
			event.FromAdmin = true
		} else {
			event.FromAdmin = false
			event.SenderID = clientID // only admin can send events not as self!
		}
		toEngine <- event
	}

	ws.Close()
}
