package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/websocket"
)

type questionnaireAnswer struct {
	ws     *websocket.Conn
	answer string
}

var toQuestionnaireWriter = make(chan questionnaireAnswer)

func questionnaireWriter(dataDirName string) {
	// create data-file
	questionnaireFile, err := os.Create(dataDirName + "/questionnaireAnswers.txt")
	if err != nil {
		fmt.Println("questionnaire error creating file:")
		panic(err)
	}
	// handle input
	for {
		answer := <-toQuestionnaireWriter
		_, err := fmt.Fprintln(questionnaireFile, answer.answer)
		if err != nil {
			fmt.Println("Error printing questionnaire answer to file:")
			fmt.Println(err)
			answer.ws.WriteJSON("fail")
			return
		}
		answer.ws.WriteJSON("success")
	}
}

func wsQuestionnaire(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	quickHandleError(err, "error upgrading questionnaire connection")

	for {
		// read answers
		var answers map[string]interface{}
		if err := ws.ReadJSON(&answers); err != nil {
			fmt.Println(err)
			ws.Close()
			return
		}

		// extract player-id
		playerID, ok := answers["playerID"]
		if !ok || playerID == "" {
			playerID = fmt.Sprint(ws.RemoteAddr())
		}
		delete(answers, "playerID")

		answers["time"] = time.Now().Format("15.04.05")

		answerString, err := json.Marshal(answers)
		quickHandleError(err, "Error Marshal'ing questionnaire answers:\n")

		toQuestionnaireWriter <- questionnaireAnswer{
			ws:     ws,
			answer: "\"" + playerID.(string) + "\":" + string(answerString),
		}
	}
}

// DECLARED IN "controlQuestionTracking.go":
// func quickHandleError(err error, context string) {
// 	if err != nil {
// 		fmt.Println(context)
// 		fmt.Println(err)
// 	}
// }
