/*jshint esversion: 6 */

window.onload = function() {
    let ws = new WebSocket("wss://" + window.location.host + "/wsQuestionnaire");
    
    ws.onclose = function() {
        document.getElementById("NoConnectionPopup").style.display = "block";
    }

    ws.onopen = function() {
        document.getElementById("NoConnectionPopup").style.display = "none";
    }

    let templatesDIV = document.getElementById("templates");

    // ADD HEXACO ANSWER OPTIONS
    let hexacoAnswerTDList = document.getElementsByClassName("hexacoAnswerTD");
    let hexacoOptionsTemplate = templatesDIV.getElementsByClassName("hexacoOptions")[0];
    for (let i = 0; i < hexacoAnswerTDList.length; i++) {
        let hexacoOptions = hexacoOptionsTemplate.cloneNode(true);
        for (let j = 0; j < hexacoOptions.children.length; j++) {
            let radioButton = hexacoOptions.children[j].children[1];
            radioButton.name = radioButton.name + i;
        }
        hexacoAnswerTDList[i].appendChild(hexacoOptions);
    }

    // ADD PLAYER-SCORE-TRs
    let playerScoreTable = document.getElementById("playerScoreTable");
    let playerScoreTBody = playerScoreTable.getElementsByTagName("tbody")[0];
    let playerScoreTRtemplate = templatesDIV.getElementsByClassName("playerScoreTR")[0];
    for (let playerID = 1; playerID <= 24; playerID++) {
        let playerScoreTR = playerScoreTRtemplate.cloneNode(true);
        playerScoreTR.children[0].innerHTML = playerID;
        playerScoreTR.children[1].children[0].name += playerID;

        playerScoreTBody.appendChild(playerScoreTR);
    }

    // PREPARE SUBMIT BUTTON
    let submitButton = document.getElementById("submitButton");
    submitButton.onclick = function() {
        let answerMap = readAnswers();
        answerMap.playerID = document.cookie;

        submitButton.style.backgroundColor = "grey";
        console.log(answerMap);
        ws.send(JSON.stringify(answerMap));
        ws.onmessage = function(msg) {
            let error = JSON.parse(msg.data);
            if (error !== "success") {
                alert("Please contact a lab assistant!" + "\n" + "server msg: " + error);
                return;
            }

            submitButton.style.backgroundColor = "";
            if (!document.getElementById("missingAnswersMsg").classList.contains("alertShown")) {
                // the above is shitty code for "All questions are answered"!
                document.getElementById("SuccesfullySubmittedPopup").style.display = "block";
            }

            scrollTo(0, 0);
        };
    };
};

function readAnswers() {
    let questionList = document.getElementsByClassName("question");
    let answerMap = {};

    let missingAnswers = 0;

    for (let q = 0; q < questionList.length; q++) {
        // read answer:
        let questionDIV = questionList[q];
        let answer = "";
        switch (questionList[q].getAttribute("data-answerType")) {
            case "radio":
                let radioButtonList = questionDIV.getElementsByTagName("input");
                for (let i = 0; i < radioButtonList.length; i++) {
                    if (radioButtonList[i].checked) {
                        answer = radioButtonList[i].value;
                        break;
                    }
                }
                break;

            case "number":
                answer = questionDIV.getElementsByTagName("input")[0].value;
                if (answer !== "") { answer = Number(answer); }
                break;

            case "textarea":
                answer = questionDIV.getElementsByTagName("textarea")[0].value;
                break;

            case "tableRadio":
                let rowList1 = questionDIV.getElementsByTagName("tr");
                answer = [];

                for (let i = 0; i < rowList1.length; i++) {
                    let radioButtonList = rowList1[i].getElementsByTagName("input");
                    let subAnswer = "";
                    for (let i = 0; i < radioButtonList.length; i++) {
                        if (radioButtonList[i].checked) {
                            subAnswer = radioButtonList[i].value;
                            break;
                        }
                    }
                    answer.push(subAnswer);
                    if (subAnswer === "") {
                        rowList1[i].classList.add("missingAnswer");
                        missingAnswers++;
                    } else {
                        rowList1[i].classList.remove("missingAnswer");
                    }
                }
                break;

            case "tableNumber":
                let rowList2 = questionDIV.getElementsByTagName("tr");
                answer = [];
                for (let i = 0; i < rowList2.length; i++) {
                    if (rowList2[i].classList.contains("playerScoreTR")) {
                        answer.push(Number(rowList2[i].getElementsByTagName("input")[0].value));
                    }
                }
                break;

            default:
        }
        if (answer === "") {
            questionDIV.classList.add("missingAnswer");
            missingAnswers++;
        } else {
            questionDIV.classList.remove("missingAnswer");
        }
        answerMap[questionDIV.id] = answer;
    }

    document.getElementById("missingAnswersSpan").innerHTML = missingAnswers;
    if (missingAnswers > 0) {
        document.getElementById("missingAnswersMsg").style.display = "block";
        document.getElementById("missingAnswersMsg").classList.add("alertShown");
    } else {
        document.getElementById("missingAnswersMsg").style.display = "none";
        document.getElementById("missingAnswersMsg").classList.remove("alertShown");
    }
    return answerMap;
}

// Old version, before "required question"
//function readAnswers() {
//    let answerMap = {};
//
//    // collect inputs
//    let inputList = document.getElementsByTagName("input");
//    for (let i = 0; i < inputList.length; i++) {
//        let answerDOM = inputList[i];
//
//        switch (answerDOM.type) {
//            case "radio":
//                if (answerDOM.checked) {
//                    answerMap[answerDOM.name] = answerDOM.value;
//                }
//                break;
//
//            default:
//                answerMap[answerDOM.name] = answerDOM.value;
//        }
//    }
//
//    let textareaList = document.getElementsByTagName("textarea");
//    for (let i = 0; i < textareaList.length; i++) {
//        let textareaDOM = textareaList[i];
//        answerMap[textareaDOM.name] = textareaDOM.value;
//    }
//    return answerMap;
//}