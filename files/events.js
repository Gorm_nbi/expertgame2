/*jshint esversion: 6 */

// game event id-table
const
    EventMsgInquiry = 1,
    EventMsgReply = 2,
    EventNoMoreMsgs = 3,

    EventNextRound = 11,
    EventNextStage = 12,

    EventStartTimer = 21,
    EventStartQuestionnaire = 22, // This could also be called "end game"

    EventClick = 31,

    EventAddPlayer = 101,
    EventShufflePlayerLists = 102,
    EventSuggestContact = 111,
    EventRemoveAllContactSuggestions = 112;


// click event id-table
const
    ClickedSendButton = 1,
    ClickedNextRoundButton = 2,

    ClickedOther = 10,

    // contact labels 1001 to 1999 (eg 1005: player-id 5 contact-label  
    ClickedContactLabel = 1000,
    // willSendButton 2001 to 2999 (eg 2005: player-id 5 willSendButton 
    ClickedWillSendIcon = 2000;

//  [NB: player-id's generally doesn't match contact names in the player interface!]
