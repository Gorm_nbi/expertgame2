/*jshint esversion: 6 */

const
//  Special playerID's
    PlayerIDAdmin = 65534,
    PlayerIDAll = 65535,

    // eventID meanings
    EventMsgInquiry = 1,
    EventMsgReply = 2,
    EventNoMoreMsgs = 3,

    EventNextRound = 11,
    EventNextStage = 12,
    EventStartTimer = 21,

    EventAddPlayer = 101,
    EventShuffleContactLists = 102;


var playerMap = {};
var Nplayers = 0;

var settings = {
    autoStartRound: 0,
    autoStartTimer: {
        nWaiting: 0,
        seconds: 15,
    },
};

var gameState = {
    currentRound: 0,
    currentStage: 0
};

var plingSound = new Audio('../Electronic_Chime-KevanGC-495939803.mp3');

window.onload = function() {
    var playerTable = document.getElementById("PlayerTable");
    ws = new WebSocket("wss://" + window.location.host + "/wsAdmin");

    ws.onclose = function() {
        document.getElementById("ConnectionStatus").innerHTML = "Disconnected... please reload!";
        document.getElementById("ConnectionStatus").style.backgroundColor = "#FF0000";
    };

    ws.onopen = function() {
        document.getElementById("ConnectionStatus").innerHTML = "Connected <span style='font-size:150%;'>&#10003</span>";
        document.getElementById("ConnectionStatus").style.backgroundColor = "#00AA00";

        // handle login
        var LoginPopup = document.getElementById("LoginPopup");
        LoginPopup.style.display = "block";
        document.getElementById("PasswordInput").focus();
        document.getElementById("PasswordSubmit").onclick = function() {
            ws.send(JSON.stringify(document.getElementById("PasswordInput").value));
            LoginPopup.style.display = "none";
        };
        document.getElementById("PasswordInput").onkeydown = function(event) {
            if (event.keyCode == 13)
                document.getElementById('PasswordSubmit').click();
        };

        // start sending events by pressing submit button
        document.getElementById("EventSubmit").onclick = function() {
            let input = document.getElementById("ActionPannel").children;
            switch (parseInt(input[0].value)) {
                case 111:
                    suggestFullInfoGraph()
                    return

                case 112:
                    unsuggestAllContacts()
                    return

                default:
                    var event = {
                        EventID: parseInt(input[0].value),
                        SenderID: parseInt(input[1].value),
                        ReceiverID: parseInt(input[2].value),
                        Reward: parseInt(input[3].value)
                    };
                    if (event.EventID === parseInt(EventStartTimer)) { // ugly stage-sync hack!!
                        event.Porto = gameState.currentStage;
                    }
                    console.log("sending event:", JSON.stringify(event));
                    ws.send(JSON.stringify(event));
            }
        };
        console.log("Ready to start sending events");
    };

    ws.onmessage = function(msg) {
        var event = JSON.parse(msg.data);

        append2EventLog(event);

        // implement event
        switch (event.EventID) {
            case EventMsgInquiry:
                playerMap[event.SenderID].incrementReward(-event.Porto);
                playerMap[event.ReceiverID].incrementReward(event.Reward);
                break;

            case EventMsgReply:
                playerMap[event.SenderID].incrementReward(-event.Porto);
                playerMap[event.ReceiverID].incrementReward(event.Reward);
                break;

            case EventNoMoreMsgs:
                playerMap[event.SenderID].setAwaitingMsg(false);
                // Auto-start timer?
                let awaiting = 0;
                for (let id in playerMap) {
                    if (playerMap[id].awaitingMsg) {
                        awaiting += 1;
                    }
                }
                if (awaiting === settings.autoStartTimer.nWaiting && awaiting > 0) {
                    let event = {
                        "EventID": EventStartTimer,
                        "Reward": settings.autoStartTimer.seconds,
                        "Porto": gameState.currentStage
                    };
                    ws.send(JSON.stringify(event));
                }
                break;

            case EventNextStage:
                for (let playerID in playerMap) {
                    playerMap[playerID].setAwaitingMsg(true);
                }
                gameState.currentStage++;
                document.getElementById("CurrentStageSpan").innerHTML = gameState.currentStage;
                // Auto-start next round?
                if (gameState.currentStage === 3 && settings.autoStartRound > 0) {
                    x = Math.random() * 100;
                    if (x < settings.autoStartRound) {
                        ws.send('{"EventID":' + EventNextRound + '}');
                    } else { console.log("Next round NOT auto-started on purpose"); }
                }

                plingSound.play();
                break;

            case EventNextRound:
                let player = playerMap[event.ReceiverID];
                player.setQuestion(event.Question);
                player.setExpertise(event.Expertise);
                player.incrementReward(event.Reward);
                player.setAwaitingMsg(false);
                if (gameState.currentStage === 3) {
                    gameState.currentRound++;
                    document.getElementById("CurrentRoundSpan").innerHTML = gameState.currentRound;
                } else if (gameState.currentRound === 0) {
                    // when the session starts
                    gameState.currentRound++;
                    document.getElementById("CurrentRoundSpan").innerHTML = gameState.currentRound;
                    actionSelectorGameStart();
                }
                gameState.currentStage = 0;
                // Shuffle contact list mode:
                if (document.getElementById("AutoShuffleContatcsCheckBox").checked) {
                    ws.send('{"EventID":' + EventShuffleContactLists + '}');
                }
                break;

            case EventAddPlayer:
                playerMap[event.SenderID] = new Player(event.SenderID, playerTable.insertRow(-1), event.Reward);
                Nplayers++
                // Auto-start timer selector:
                autoStartTimerAddPlayer();
        }
    };
};

function append2EventLog(event) {
    var eventLog = document.getElementById("EventLog");
    var newLogEntry = document.createElement("p");
    var eventText = "";

    if (typeof(event) != "object") {
        return;
    }
    switch (event.EventID) {
        case 1:
            eventText += "Inquiry" + "&nbsp;&nbsp;&nbsp;&nbsp;";
            break;
        case 2:
            eventText += "Reply" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            break;
        case 3:
            eventText += "NoMoreMsgs" + "&nbsp;";
            break;
        case 11:
            eventText += "NextRound" + "&nbsp;&nbsp;";
            break;
        case 12:
            eventText += "NextStage" + "&nbsp;";
            break;
        case 21:
            eventText += "StartTimer" + "&nbsp;";
            break;
        case 22:
            eventText += "StartQuestionnaire" + "&nbsp;";
            break;
        case 101:
            eventText += "AddPlayer" + "&nbsp;&nbsp;";
            break;
        default:
            eventText += "unknown event-id: " + event.EventID + "&nbsp;&nbsp;";
    }

    if (event.SenderID !== 0) {
        eventText += "sender:" + event.SenderID + "&nbsp;&nbsp;";
    }
    if (event.ReceiverID !== 0) {
        eventText += "receiver:";
        if (event.ReceiverID === 65535) eventText += "ALL" + "&nbsp;&nbsp;";
        else eventText += event.ReceiverID + "&nbsp;&nbsp;";
    }
    if (event.ReferredID !== 0) {
        eventText += "referred:" + event.ReferredID + "&nbsp;&nbsp;";
    }
    if (event.Question !== 0) {
        eventText += "question:" + event.Question + "&nbsp;&nbsp;";
    }
    if (event.Expertise !== 0) {
        eventText += "expertise:" + event.Expertise + "&nbsp;&nbsp;";
    }
    if (event.Porto !== 0) {
        eventText += "porto:" + event.Porto + "&nbsp;&nbsp;";
    }
    if (event.Reward !== 0) {
        eventText += "reward:<span style='color:#FFDD00'>" + event.Reward + "</span>&nbsp;&nbsp;";
    }
    if (event.FromAdmin) {
        eventText += "<span style='color:#FFDD00'>Admin</span>" + "&nbsp;&nbsp;";
    }
    if (!event.Valid) {
        eventText += "<span style='color:red'>Invalid</span>" + "&nbsp;&nbsp;";
    }
    eventText += event.Time;

    newLogEntry.innerHTML = eventText;
    eventLog.appendChild(newLogEntry);
}

function Player(playerID, playerRow, initReward) {
    // build row
    this.Row = playerRow;
    this.idCell = this.Row.insertCell(0);
    this.questionCell = this.Row.insertCell(1);
    this.expertiseCell = this.Row.insertCell(2);
    this.rewardCell = this.Row.insertCell(3);
    this.awaitingMsgCell = this.Row.insertCell(4);

    this.idCell.innerHTML = playerID;

    this.setQuestion = function(question) {
        this.question = question;
        this.questionCell.innerHTML = this.question;
    };
    this.setQuestion(0);

    this.setExpertise = function(expertise) {
        this.expertise = expertise;
        this.expertiseCell.innerHTML = this.expertise;
    };
    this.setExpertise(0);

    this.reward = 0;
    this.incrementReward = function(rewardChange) {
        this.reward += rewardChange;
        this.rewardCell.innerHTML = this.reward;
    };
    this.incrementReward(initReward);

    this.setAwaitingMsg = function(awaitingMsg) {
        this.awaitingMsg = awaitingMsg;
        this.awaitingMsgCell.innerHTML = this.awaitingMsg;
    };
    this.setAwaitingMsg(false);
}

function actionSelector() {
    let eventID = parseInt(document.getElementById("InputEventID").value);
    let parameterInputs = document.getElementById("ActionPannel").getElementsByTagName("input");

    switch (eventID) {
        case EventMsgInquiry:
        case EventMsgReply:
            parameterInputs[0].style.display = "initial";
            parameterInputs[1].style.display = "initial";
            //parameterInputs[1].value = "";
            parameterInputs[2].style.display = "none";
            parameterInputs[2].value = "";
            break;

        case EventStartTimer:
            parameterInputs[0].style.display = "none";
            parameterInputs[0].value = "";
            parameterInputs[1].style.display = "none";
            parameterInputs[1].value = "";
            parameterInputs[2].style.display = "initial";
            break;

        default:
            for (let i = 0; i < parameterInputs.length; i++) {
                parameterInputs[i].style.display = "none";
                parameterInputs[i].value = "";
            }
    }
}

function actionSelectorGameStart() {
    let actionSelectorElem = document.getElementById("InputEventID");
    actionSelectorElem.remove(0);
    actionSelectorElem.options[0].innerHTML = "Next round";
    actionSelectorElem.appendChild(actionSelectorElem.options[0]);
    actionSelectorElem.options[0].selected = true;
    actionSelector();
}

function autoStartTimerAddPlayer() {
    let selector = document.getElementById("timerNWaitingSelector");
    let newOption = selector.options[selector.options.length - 1].cloneNode(true);
    newOption.value++;
    newOption.innerHTML = newOption.value;
    selector.appendChild(newOption);
}

function suggestContact(receiverID, referredID) {
    ws.send(JSON.stringify({EventID:111, ReceiverID:receiverID, ReferredID:referredID}));
}

function unsuggestAllContacts() {
    ws.send(JSON.stringify({EventID:112, ReceiverID:65535}));
}

function suggestFullInfoGraph() {
    // This function assumes that the game has 25 players!!
    if (Nplayers != 25) {
        console.log("ERROR: the fully connected graph can only make sense for exactly 25 players!")
        return
    }
    // else
    for (ring = 0; ring<5; ring++) {
        for (j = 0; j<5; j++) {
            // assign neighboutrs within the ring
            suggestContact(ring*5+j + 1, ring*5+(j+1)%5 + 1)
            suggestContact(ring*5+j + 1, ring*5+(j-1+5)%5 + 1)
            // assign a neighbour from each of the four other rings:
            suggestContact(ring*5+j + 1, ((ring+1)%5)*5 + (2*j+1)%5 + 1)
            suggestContact(ring*5+j + 1, ((ring+2)%5)*5 + (3*j+3)%5 + 1)
            suggestContact(ring*5+j + 1, ((ring+3)%5)*5 + (2*j+4)%5 + 1)
            suggestContact(ring*5+j + 1, ((ring+4)%5)*5 + (3*j+2)%5 + 1)
        }
    }
}