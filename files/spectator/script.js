/*jshint esversion: 6 */

const
//  Special playerID's
    PlayerIDAdmin = 65534,
    PlayerIDAll = 65535,

    // eventID meanings
    EventMsgInquiry = 1,
    EventMsgResponse = 2,
    EventNoMoreMsgs = 3,

    EventNextGame = 11,
    EventNextRound = 12,

    EventAddPlayer = 101;


var playerMap = {};

window.onload = function() {
    var playerTable = document.getElementById("PlayerTable");
    var ws = new WebSocket("wss://" + window.location.host + "/wsSpectator");

    ws.onclose = function() {
        document.getElementById("ConnectionStatus").innerHTML = "Disconnected... please reload!";
        document.getElementById("ConnectionStatus").style.backgroundColor = "#FF0000";
    };

    ws.onopen = function() {
        document.getElementById("ConnectionStatus").innerHTML = "Connected <span style='font-size:150%;'>&#10003</span>";
        document.getElementById("ConnectionStatus").style.backgroundColor = "#00AA00";
    };

    ws.onmessage = function(msg) {
        var event = JSON.parse(msg.data);

        append2EventLog(event);

        // implement event
        switch (event.EventID) {
            case EventMsgInquiry:
                playerMap[event.SenderID].incrementReward(-event.Porto);
                playerMap[event.ReceiverID].incrementReward(event.Reward);
                break;

            case EventMsgResponse:
                playerMap[event.SenderID].incrementReward(-event.Porto);
                playerMap[event.ReceiverID].incrementReward(event.Reward);
                break;

            case EventNoMoreMsgs:
                playerMap[event.SenderID].setAwaitingMsg(false);
                break;

            case EventNextRound:
                for (let playerID in playerMap) {
                    playerMap[playerID].setAwaitingMsg(true);
                }
                break;

            case EventNextGame:
                let player = playerMap[event.ReceiverID];
                player.setQuestion(event.Question);
                player.setExpertise(event.Expertise);
                player.incrementReward(event.Reward);
                player.setAwaitingMsg(false);
                break;

            case EventAddPlayer:
                playerMap[event.SenderID] = new Player(event.SenderID, playerTable.insertRow(-1), event.Reward);
        }
    };
};

function append2EventLog(event) {
    var eventLog = document.getElementById("EventLog");
    var newLogEntry = document.createElement("p");
    var eventText = "";

    if (typeof(event) != "object") {
        return;
    }
    switch (event.EventID) {
        case 1:
            eventText += "Inquiry" + "&nbsp;&nbsp;&nbsp;&nbsp;";
            break;
        case 2:
            eventText += "Reply" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            break;
        case 3:
            eventText += "NoMoreMsgs" + "&nbsp;";
            break;
        case 11:
            eventText += "NextGame" + "&nbsp;&nbsp;";
            break;
        case 12:
            eventText += "NextRound" + "&nbsp;";
            break;
        case 101:
            eventText += "AddPlayer" + "&nbsp;&nbsp;";
            break;
        default:
            eventText += "unknown event-id: " + event.EventID + "&nbsp;&nbsp;";
    }

    if (event.SenderID !== 0) {
        eventText += "sender:" + event.SenderID + "&nbsp;&nbsp;";
    }
    if (event.ReceiverID !== 0) {
        eventText += "receiver:";
        if (event.ReceiverID === 65535) eventText += "ALL" + "&nbsp;&nbsp;";
        else eventText += event.ReceiverID + "&nbsp;&nbsp;";
    }
    if (event.ReferredID !== 0) {
        eventText += "referred:" + event.ReferredID + "&nbsp;&nbsp;";
    }
    if (event.Question !== 0) {
        eventText += "question:" + event.Question + "&nbsp;&nbsp;";
    }
    if (event.Expertise !== 0) {
        eventText += "expertise:" + event.Expertise + "&nbsp;&nbsp;";
    }
    if (event.Porto !== 0) {
        eventText += "porto:" + event.Porto + "&nbsp;&nbsp;";
    }
    if (event.Reward !== 0) {
        eventText += "reward:<span style='color:#FFDD00'>" + event.Reward + "</span>&nbsp;&nbsp;";
    }
    if (event.FromAdmin) {
        eventText += "<span style='color:#FFDD00'>Admin</span>" + "&nbsp;&nbsp;";
    }
    if (!event.Valid) {
        eventText += "<span style='color:red'>Invalid</span>" + "&nbsp;&nbsp;";
    }
    eventText += event.Time;

    newLogEntry.innerHTML = eventText;
    eventLog.appendChild(newLogEntry);
}

function Player(playerID, playerRow, initReward) {
    // build row
    this.Row = playerRow;
    this.idCell = this.Row.insertCell(0);
    this.questionCell = this.Row.insertCell(1);
    this.expertiseCell = this.Row.insertCell(2);
    this.rewardCell = this.Row.insertCell(3);
    this.awaitingMsgCell = this.Row.insertCell(4);

    this.idCell.innerHTML = playerID;

    this.setQuestion = function(question) {
        this.question = question;
        this.questionCell.innerHTML = this.question;
    };
    this.setQuestion(0);

    this.setExpertise = function(expertise) {
        this.expertise = expertise;
        this.expertiseCell.innerHTML = this.expertise;
    };
    this.setExpertise(0);

    this.reward = 0;
    this.incrementReward = function(rewardChange) {
        this.reward += rewardChange;
        this.rewardCell.innerHTML = this.reward;
    };
    this.incrementReward(initReward);

    this.setAwaitingMsg = function(awaitingMsg) {
        this.awaitingMsg = awaitingMsg;
        this.awaitingMsgCell.innerHTML = this.awaitingMsg;
    };
    this.setAwaitingMsg(false);
}
