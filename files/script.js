/*jshint esversion: 6 */

var ownID = 0;
var moreMsgs = false;
var ownReward = 0;

// the templates div contain all the templates to build the page.
var templates = document.createElement("div");

// Info about each player is stored in this map
var contacts = {}; // map[playerID]contacts

// All relevant information about game events.
var messages = {
    allMsgs: [1, 2, 3, 4],
    msgLog: document.createElement("div"),
    currentRound: document.createElement("div"),
    currentStage: document.createElement("div")
};

var currentRound = 0;
var currentStage = 0;

// The event-queue is implemented to facilitate a better transition between rounds
var eventQueue = {
    waitingMsgs: false,
    list: [],
    open: function(event) {
        this.waitingMsgs = true;
        this.list = [event];
        document.getElementById("EmptyEventQueue").style.display = "inline";
        document.getElementById("EmptyEventQueue").onclick = function(e) {
            eventQueue.empty();
            // register click:
            logClickEvent(ClickedNextRoundButton);
            e.stopPropagation();
        };
        document.getElementById("OwnInfo").style.backgroundColor = "#587094";
    },
    empty: function() {
        // when the event-queue is empty it cannot be emptied again.
        document.getElementById("EmptyEventQueue").onclick = function() {};
        document.getElementById("EmptyEventQueue").style.display = "none";
        for (let i in this.list) {
            handleEvent(this.list[i]);
        }
        this.waitingMsgs = false;
        this.list = [];
    }
};

var plingSound = new Audio('Electronic_Chime-KevanGC-495939803.mp3');


// ****************************************************************************
//  Main function
// ****************************************************************************
window.onload = function() {
    var ws = new WebSocket("wss://" + window.location.host + "/wsPlayer");
    window.onresize = function() { resizeScreen(); };
    resizeScreen();

    templates = document.getElementById("Templates");
    messages.msgLog = document.getElementById("MsgLog");

    ws.onclose = function() {
        var popups = document.getElementsByClassName("PopupCover");
        for (i = 0; i < popups.length; i++) {
            if (popups[i].id === "LostConnectionPopup") {
                popups[i].style.display = "block";
            } else {
                popups[i].style.display = "none";
            }
        }
    };

    ws.onopen = function() {
        { // send login request
            var loginPopup = document.getElementById("LoginPopup");
            var loginInput = document.getElementById("LoginInput");
            var loginButton = document.getElementById("LoginButton");

            loginPopup.style.display = "block";
            loginInput.onkeydown = function() {
                if (event.keyCode == 13) { loginButton.click(); }
            };
            loginButton.onclick = function(e) {
                ws.send(loginInput.value);
            };
        }
    };

    ws.onmessage = function(msg) {
        // First incomming message is expected to be a player-id confirmation:
        let id = JSON.parse(msg.data);
        if (typeof(id) != "number") {
            ws.close();
            console.log("Received unexpected type '" + typeof(id) + "' from ws.");
            console.log("  expected a player-id of type 'number'");
        }
        ownID = id;
        document.cookie = "playerID=" + ownID;

        { // Wait for player to click "ready to play"
            let loginPopup = document.getElementById("LoginPopup");
            let ownIdConfirmation = document.getElementById("OwnIDConfirmation");
            let skipTutorialButton = document.getElementById("skipTutorialButton");
            let startTutorialButton = document.getElementById("StartTutorialButton");

            loginPopup.children[0].style.display = "none";
            loginPopup.children[1].style.display = "block";
            ownIdConfirmation.innerHTML = ownID;

            startTutorialButton.innerHTML = "Start tutorial";
            startTutorialButton.onclick = function() {
                loginPopup.style.display = "none";
                window.open("tutorial/page1.html");
            };

            skipTutorialButton.style.display = "initial";
            skipTutorialButton.onclick = function() {
                loginPopup.style.display = "none";
            };
            //  loginPopup.style.display = "none";// uncomment this line to skip tutorial
        }

        // After the first one, messages will be treated as game-events
        ws.onmessage = function(msg) {
            let event = JSON.parse(msg.data);
            console.log("received event:", event);
            if (event.Valid) {
                // Next round-events start a new event-queue.
                // If an event-queue already exists, those events are handled first.
                if (event.EventID === EventStartTimer && event.Valid) {
                    if (eventQueue.waitingMsgs || moreMsgs) {
                        timer.start(event.Reward);
                    }

                } else if (event.EventID === EventNextRound) {
                    if (eventQueue.waitingMsgs) {
                        eventQueue.empty();
                    }
                    eventQueue.open(event);

                } else {
                    // Non-NextRound events will be handled right away
                    // or appended to the game-queue if it is active.
                    if (eventQueue.waitingMsgs) {
                        eventQueue.list.push(event);
                    } else {
                        handleEvent(event);
                    }
                }
            }
        };

        // Once the player-id is confirmed, the send-button is activated.
        document.getElementById("SendButton").onclick = function(e) {
            timer.stop();
            if (!moreMsgs) {
                console.log("The send buttons was clicked when it shouldnt have been possible!");
            }
            let nMsgs = numberOfMsgs();
            if (nMsgs > ownReward && nMsgs !== 0) {
                let notEnoughtMoneyPopup = document.getElementById("NotEnoughMoneyPopup");
                let spanList = notEnoughtMoneyPopup.getElementsByTagName("span");
                let closePopupButton = notEnoughtMoneyPopup.getElementsByClassName("PopupButton")[0];
                spanList[0].innerHTML = nMsgs;
                spanList[1].innerHTML = ownReward;
                closePopupButton.onclick = function() {
                    notEnoughtMoneyPopup.style.display = "none";
                };
                notEnoughtMoneyPopup.style.display = "block";

                return;
            }

            for (var receiverID in contacts) {
                let receiver = contacts[receiverID];
                if (receiver.willSend > 9) {
                    let event = {
                        EventID: currentStage,
                        SenderID: ownID,
                        ReceiverID: parseInt(receiverID),
                    };
                    console.log(JSON.stringify("sending:", event));
                    ws.send(JSON.stringify(event));
                }
            }

            let event = { EventID: EventNoMoreMsgs };
            ws.send(JSON.stringify(event));

            // register click:
            logClickEvent(ClickedSendButton);
            e.stopPropagation();
        };

        // and THEN click-collection begins
        logClickEvent = function(clickedID) {
            ws.send('{"EventId": 31,"Clicked":' + clickedID + "}");
            console.log("have sent:", '{"EventId": 31,"Clicked":' + clickedID + "}")
        };
    };
};

var logClickEvent = function(c) {};


// handleEvent is simply switch on event.EventID passing the event on to a 
function handleEvent(event) {
    switch (event.EventID) {

        // Only before games
        case EventAddPlayer:
            handleEventAddPlayer(event);
            break;

            // Game events
        case EventNextRound:
            handleEventNextRound(event);
            break;
        case EventNextStage:
            handleEventNextStage(event);
            break;

            // Player messages
        case EventMsgInquiry:
            handleEventMsgInquiry(event);
            break;
        case EventMsgReply:
            handleEventMsgReply(event);
            break;
        case EventNoMoreMsgs:
            handleEventNoMoreMsgs(event);
            break;

        case EventShufflePlayerLists:
            shuffleContactLabels();
            break;

        case EventSuggestContact:
            SuggestContact(event.ReferredID);
            break;

        case EventRemoveAllContactSuggestions:
            RemoveAllContactSuggestions();
            break;

        case EventStartQuestionnaire:
            let gameOverPopup = document.getElementById("GameOverPopup");
            let popupButton = gameOverPopup.getElementsByClassName("PopupButton")[0];
            popupButton.onclick = function() {
                gameOverPopup.style.display = "none";
                window.open("questionnaire");
            };
            gameOverPopup.style.display = "block";
            break;

        default:
            console.log("Ignored event because of unexpected eventID:", event);
    }
}

function handleEventAddPlayer(event) {
    if (event.SenderID === ownID) {
        handleReward(event.Reward);
    }
    contacts[event.SenderID] = new contact(event.SenderID);
    rearrangeContactLabelsCyclic();
}

function handleEventNextRound(event) {
    if (event.ReceiverID !== ownID) {
        console.log("NextRound event has wrong receiver:", event);
        return;
    }

    currentRound++;
    currentStage = 0;

    // reset contact-information
    for (var i in contacts) {
        contacts[i].setQuestion(0);
        contacts[i].setExpertise(0);
        contacts[i].clearCounter();
        contacts[i].setWillSend(1);
    }

    // Set new question and expertise for self
    contacts[ownID].setQuestion(event.Question);
    contacts[ownID].setExpertise(event.Expertise);

    // Previous round (if it exists) is no longer current:
    messages.currentRound.removeAttribute("id");

    // make new current round
    messages.currentRound = templates.getElementsByClassName("MsgLogRound")[0].cloneNode(true);
    messages.currentRound.id = "CurrentRound";

    // Write round header
    let roundID_question_expertise = messages.currentRound.getElementsByClassName("MsgLogRoundHeader")[0].children;
    roundID_question_expertise[0].innerHTML = currentRound;
    roundID_question_expertise[1].innerHTML = expertiseNames[event.Question];
    roundID_question_expertise[2].innerHTML = expertiseNames[event.Expertise];

    messages.msgLog.appendChild(messages.currentRound);
}

function handleEventNextStage(event) {
    currentStage++;
    moreMsgs = true;

    setDialogue("DialogueStage" + currentStage);

    switch (currentStage) {
        case 1:
            for (let id in contacts) { contacts[id].setWillSend(1); }
            document.getElementById("SendButton").style.display = "inline";
            document.getElementById("OwnInfo").style.backgroundColor = "#587094";
            break;

        case 2:
            for (let id in contacts) {
                if (contacts[id].willSend !== 2) {
                    contacts[id].setWillSend(-1);
                }
            }
            document.getElementById("SendButton").style.display = "inline";
            document.getElementById("OwnInfo").style.backgroundColor = "#587094";
            break;

        case 3:
            document.getElementById("SendButton").style.display = "none";
            break;
    }

    messages.currentStage = templates.getElementsByClassName("MsgLogStage")[0].cloneNode(true);
    messages.currentRound.appendChild(messages.currentStage);

    let msgLog = document.getElementById("MsgLog");
    msgLog.scrollTop = msgLog.scrollHeight;
    plingSound.play();
}

function handleEventMsgInquiry(event) {
    var msg = templates.getElementsByClassName("Msg")[0].cloneNode(true);
    var msgHeader = msg.getElementsByClassName("MsgHeader")[0];
    var msgIcon = msgHeader.getElementsByTagName("img")[0];
    var msgBody = msg.getElementsByClassName("MsgBody")[0];
    var correspondentID = 0;

    msgIcon.src = iconSRCs[10];
    msgBody.innerHTML += "I am an expert in <b>" + expertiseNames[event.Expertise] + "</b><br/>";
    msgBody.innerHTML += "I have a question about <b>" + expertiseNames[event.Question] + "</b><br/>";
    msgBody.innerHTML += "Can you help me?";

    if (event.SenderID === ownID) {
        msg.classList.add("Sent");
        correspondentID = event.ReceiverID;
        msg.correspondentID = correspondentID;

        msgHeader.getElementsByTagName("div")[0].innerHTML += "To player " + contacts[correspondentID].name;
        messages.currentStage.getElementsByClassName("MsgLogSent")[0].appendChild(msg);

        contacts[event.ReceiverID].counterSent.increment(1);
        handleReward(-event.Porto);

    } else if (event.ReceiverID === ownID) {
        msg.classList.add("Received");
        correspondentID = event.SenderID;
        msg.correspondentID = correspondentID;

        msgHeader.getElementsByTagName("div")[0].innerHTML += "From player " + contacts[correspondentID].name;
        messages.currentStage.getElementsByClassName("MsgLogReceived")[0].appendChild(msg);

        handleReward(event.Reward);

        let sender = contacts[event.SenderID];
        sender.setQuestion(event.Question);
        sender.setExpertise(event.Expertise);
        sender.counterReceived.increment(1);
        sender.setWillSend(2);
        console.log("TEMP! have set 'willSend=2' for player", sender.name)

        if (sender.expertise === contacts[ownID].question) {
            sender.counterReceived.upgrade(1);
            msgIcon.src = iconSRCs[100];
        }
    }
}

function handleEventMsgReply(event) {
    var msg = templates.getElementsByClassName("Msg")[0].cloneNode(true);
    var msgHeader = msg.getElementsByClassName("MsgHeader")[0];
    var msgIcon = msgHeader.getElementsByTagName("img")[0];
    var msgBody = msg.getElementsByClassName("MsgBody")[0];
    var correspondentID = 0;

    if (event.ReferredID === 0) {
        msgIcon.src = iconSRCs[20];
        msgBody.innerHTML += "I'm sorry, but I don't know anyone who's an expert in <b>" + expertiseNames[event.Question] + "</b>";
    } else if (event.ReferredID === event.SenderID) {
        msgIcon.src = iconSRCs[200];
        msgBody.innerHTML += "Yes! I happen to be the expert you are looking for.";
    } else {
        msgIcon.src = iconSRCs[200];
        msgBody.innerHTML += "The expert you are looking for is player " + contacts[event.ReferredID].name;
    }

    if (event.SenderID === ownID) {
        msg.classList.add("Sent");
        correspondentID = event.ReceiverID;
        msg.correspondentID = correspondentID;

        msgHeader.getElementsByTagName("div")[0].innerHTML += "To player " + contacts[correspondentID].name;
        messages.currentStage.getElementsByClassName("MsgLogSent")[0].appendChild(msg);

        handleReward(-event.Porto);

        let receiver = contacts[event.ReceiverID];
        receiver.counterSent.increment(2);

        if (event.ReferredID !== 0) {
            receiver.counterSent.upgrade(2);
        }

    } else if (event.ReceiverID === ownID) {
        msg.classList.add("Received");
        correspondentID = event.SenderID;
        msg.correspondentID = correspondentID;

        msgHeader.getElementsByTagName("div")[0].innerHTML += "From player " + contacts[correspondentID].name;
        messages.currentStage.getElementsByClassName("MsgLogReceived")[0].appendChild(msg);

        handleReward(event.Reward);

        let sender = contacts[event.SenderID];
        let referred = contacts[event.ReferredID];
        sender.counterReceived.increment(2);
        if (referred !== undefined) {
            sender.counterReceived.upgrade(2);
            referred.setExpertise(event.Expertise);
        }
    }
}

function handleEventNoMoreMsgs(event) {
    if (event.SenderID !== ownID) {
        console.log("Non-expected event:", event);
        return;
    }

    moreMsgs = false;
    document.getElementById("SendButton").style.display = "none";
    document.getElementById("OwnInfo").style.backgroundColor = "#888888";

    for (var receiverID in contacts) {
        let receiver = contacts[receiverID];
        if (currentStage === 2 || receiver.willSend !== 2) {
            receiver.setWillSend(-1);
        }
    }

    setDialogue("DialogueNoMoreMsgs");

    let msgLog = document.getElementById("MsgLog");
    msgLog.scrollTop = msgLog.scrollHeight;
}

function contact(playerID) {
    let c = this; // needed because a button.onclick call overwrites 'this'

    if (playerID === ownID) {
        // OwnQuestion
        this.setQuestion = function(question) {
            this.question = question;
            document.getElementById("OwnQuestion").innerHTML = expertiseNames[question];
        };
        this.setQuestion(0);

        // OwnExpertise
        this.setExpertise = function(expertise) {
            this.expertise = expertise;
            document.getElementById("OwnExpertise").innerHTML = expertiseNames[expertise];
        };
        this.setExpertise(0);

        contacts[playerID] = this;

        this.setWillSend = function() {};
        this.clearCounter = function() {};
        return;
    }

    // Init label:
    this.label = templates.getElementsByClassName("ContactLabel")[0].cloneNode(true);
    this.label.id = "ContactLabel" + playerID;
    document.getElementById("ContactList").appendChild(this.label);

    // Name
    this.setName = function(nameId) {
        this.name = contactNames[nameId];
        this.label.getElementsByClassName("ContactName")[0].innerHTML = this.name;
    };
    this.setName(0);

    // Question
    this.setQuestion = function(question) {
        this.question = question;
        this.label.getElementsByClassName("ContactQuestion")[0].innerHTML = expertiseNames[question];
    };
    this.setQuestion(0);

    // Expertise
    this.setExpertise = function(expertise) {
        this.expertise = expertise;
        this.label.getElementsByClassName("ContactExpertise")[0].innerHTML = expertiseNames[expertise];
    };
    this.setQuestion(0);

    // Will send
    this.willSend = 0;
    this.willSendIcon = this.label.children[5];
    this.setWillSend = function(i) {
        if (i < 0) {
            this.willSendIcon.style.display = "none";
        } else {
            this.willSendIcon.src = iconSRCs[i];
            this.willSendIcon.style.display = "inline";
        }
        this.willSend = i;
    };
    this.setWillSend(-1);

    // Message counters
    function msgCounter(msgCounterHTML) {
        this.counter = {
            1: msgCounterHTML.children[1],
            2: msgCounterHTML.children[2]
        };
        this.icon = {
            1: msgCounterHTML.children[0],
            2: msgCounterHTML.children[3]
        };
        this.nextRound = function() {
            this.icon[1].src = iconSRCs[1];
            this.icon[2].src = iconSRCs[2];
        };
        this.increment = function(EventID) {
            this.counter[EventID].innerHTML = parseInt(this.counter[EventID].innerHTML) + 1;
            this.icon[EventID].src = iconSRCs[EventID * 10];
        };
        this.upgrade = function(EventID) {
            this.icon[EventID].src = iconSRCs[EventID * 100];
        };
        this.reset = function() {
            this.counter[1].innerHTML = 0;
            this.counter[2].innerHTML = 0;
        };
    }
    this.counterSent = new msgCounter(this.label.getElementsByClassName("MsgCounter")[0]);
    this.counterReceived = new msgCounter(this.label.getElementsByClassName("MsgCounter")[1]);
    this.clearCounter = function() {
        this.counterSent.nextRound();
        this.counterReceived.nextRound();
    };
    this.resetCounters = function() {
        this.counterSent.reset();
        this.counterReceived.reset();
    };

    this.willSendIcon.onmousedown = function(e) {
        if (c.willSend > 9) {
            c.setWillSend(currentStage);
        } else {
            if (expertKnown(c.question)) {
                c.setWillSend(currentStage * 100);
            } else {
                c.setWillSend(currentStage * 10);
            }
        }
        // register click:
        logClickEvent(ClickedWillSendIcon + playerID);
        e.preventDefault();
    };

    // prevent the default click-logging
    this.willSendIcon.onclick = function(e) { e.stopPropagation(); };

    contacts[playerID] = this;

    this.label.onfocus = function() {
        document.getElementById("MsgLogHeader").innerHTML = "All message exchanged with player " + c.name;
        let msgs = messages.msgLog.getElementsByClassName("Msg");
        for (let i = 0; i < msgs.length; i++) {
            if (msgs[i].correspondentID === playerID) {
                //msgs[i].style.display = "initial";
            } else {
                msgs[i].style.display = "none";
            }
        }
        // register click:
        logClickEvent(ClickedContactLabel + playerID);
    };
    this.label.onblur = function() {
        document.getElementById("MsgLogHeader").innerHTML = "Archive of all messages sent and received";
        let msgs = messages.msgLog.getElementsByClassName("Msg");
        for (let i = 0; i < msgs.length; i++) {
            msgs[i].style.display = "block";
        }
    };

    // prevent the default click-logging
    this.label.onclick = function(e) { e.stopPropagation(); };
}

function rearrangeContactLabelsCyclic() {
    // This will rearrange the contact-labels cyclically according
    // to the order in which they are added
    // It will also rename the contacts in numeric order
    let contactList = document.getElementById("ContactList");
    let Ncontacts = 0;

    for (let id in contacts) {
        if (parseInt(id) < ownID) {
            contactList.append(contacts[id].label);
        }
        Ncontacts++;
    }
    for (let id in contacts) {
        let i = parseInt(id);
        if (i !== ownID) {
            contacts[id].setName((i + Ncontacts - ownID) % Ncontacts);
        }
    }
}

function shuffleContactLabels() {
    let contactList = document.getElementById("ContactList");
    let newOrder = shuffle(Object.keys(contacts));
    let name = 1;
    for (let i = 0; i < newOrder.length; i++) {
        let id = newOrder[i];
        if (id !== ownID + "") {
            contacts[id].setName(name++);
            contacts[id].resetCounters();
            contactList.append(contacts[id].label);
        }
    }
    // hide history
    let pastMessages = document.getElementsByClassName("Msg");
    for (let i = 0; i < pastMessages.length; i++) {
        console.log(pastMessages[i].children[0].children[1].innerHTML)
        if (pastMessages[i].children[0].children[1].innerHTML !== "") {
            pastMessages[i].style.display = "none";
        }
    }
}

function handleReward(x) {
    ownReward += x;
    document.getElementById("RewardLabel").children[0].innerHTML = ownReward;
}

function setDialogue(DialogueID) {
    let dialogueWindow = document.getElementById("DialogueWindow");
    let dialogues = dialogueWindow.getElementsByTagName("p");
    for (var i = 0; i < dialogues.length; i++) {
        dialogues[i].style.display = "none";
    }
    dialogues[DialogueID].style.display = "block";
}

function resizeScreen() {
    let windowHeight = document.getElementById("labScreen").offsetHeight;
    let headerHeight = document.getElementById("OwnInfo").offsetHeight;
    let msgLogHeaderHeight = document.getElementById("MsgLogHeader").offsetHeight;
    let dialogueHeight = document.getElementById("DialogueWindow").offsetHeight;

    let msgLogHeight = windowHeight - headerHeight - msgLogHeaderHeight - dialogueHeight - 25;

    document.getElementById("MsgLog").style.height = msgLogHeight + "px";
}

function expertKnown(question) {

    if (question === 0) { return false; } // question is not even known

    for (let id in contacts) {
        if (contacts[id].expertise === question) {
            return true;
        }
    }

    return false;
}

function numberOfMsgs() {
    var nMsgs = 0;
    for (var receiverID in contacts) {
        let receiver = contacts[receiverID];
        if (receiver.willSend > 9) {
            nMsgs += 1;
        }
    }
    return nMsgs;
}

// **********************
//    Click collection
// **********************
document.addEventListener("click", function() {
    logClickEvent(ClickedOther);
});


// ***************************
//  Contact Suggestion Labels
// ***************************
function SuggestContact(playerID) {
    let star = templates.getElementsByClassName("ContactSuggestionLabel")[0].cloneNode(true);
    contacts[playerID].label.appendChild(star);
    // open suggestion start explanation pop-up
    let popup = document.getElementById("suggestionStarExplanationPopup");
    let popupButton = popup.getElementsByClassName("PopupButton")[0];
    popup.style.display = "block";
    popupButton.onclick = function() {
        popup.style.display = "none";
    };
}

function RemoveAllContactSuggestions() {
    for (let playerID in contacts) {
        if (contacts.hasOwnProperty(playerID) && playerID != ownID) {           
            let label = contacts[playerID].label;
            let stars = label.getElementsByClassName("ContactSuggestionLabel");
            for (let i=stars.length-1; i>=0; i--) {
                label.removeChild(stars[i]);
            }
        }
    }
}


// **************************
// Should be standard library
// **************************

function shuffle(arr) {
    for (let i = arr.length; --i > 0;) {
        let j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    return arr;
}