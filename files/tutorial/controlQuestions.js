/* jshint esversion: 6 */

window.onload = function() {
    // notice that it's global!
    ws = new WebSocket("wss://" + window.location.host + "/wsControlQuestions");

    ws.onclose = function() {
        document.getElementById("NoConnectionPopup").style.display = "block";
    }

    ws.onopen = function() {
        document.getElementById("NoConnectionPopup").style.display = "none";
    }
};

function anounceProgress(progress) {
    ws.send("{playerID:" + document.cookie + "," + progress + "}");
}

function submitAllAnswers() {
    let answerList = document.getElementsByClassName("answer");
    let answerMap = {};

    // generate usefull answerMap
    for (let i = 0; i < answerList.length; i++) {
        let answer = answerList[i];
        answerMap[answer.name] = answer.value;
    }

    let allCorrect = true;
    for (let name in answerMap) {
        if (answerMap[name] === correctAnswers[name]) {
            answerList[name].parentElement.className = "correct";
        } else {
            allCorrect = false;
            answerList[name].parentElement.className = "wrong";
        }
    }

    // append playerID and send answers
    answerMap.playerID = document.cookie;
    ws.send(JSON.stringify(answerMap));
    ws.onmessage = function(msg) {
        let error = JSON.parse(msg.data);
        if (error !== "success") {
            alert("Please contact a lab assistant!" + "\n" + "server msg: " + error);
            return;
        }

        console.log("err: ", error)
        console.log("allCorrect: ", allCorrect)
        if (allCorrect) {
            document.getElementById("nextButton").click();
        } else {
            let headline = document.getElementsByTagName("h3")[0];

            headline.innerHTML = "Your answers to questions marked with &#x2718; are wrong. Please select the correct answers, and submit again.";
        }
    };
}


var correctAnswers = {
    "q1": "V",
    "q2": "6",
    "q3": "2",
    "q4": "5",
    "q5": "yes",
    "q6": "no",
    "q7": "yes",
    "q8": "no",
    "q9": "no",
    "q10": "2",
    "q11": "3",
    "q12": "2",
    "q13": "95",
    "q14": "15",
    "q15": "105",
    "q16": "15"
};