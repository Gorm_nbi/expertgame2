// This java-script object controles the timer:
var timer = {
    timerDIV: document.getElementById("Timer"),
    timeLeftSPAN: document.getElementById("TimeLeft"),

    setIntervalID: 0,

    secondsLeft: -1,

    tictoc: function() {
        timer.secondsLeft -= 1;
        timer.timeLeftSPAN.innerHTML = timer.secondsLeft;
        console.log("Timer started.setIntervalID =", timer.setIntervalID);

        // when to stop
        if (timer.secondsLeft === 0) {
            document.getElementById("EmptyEventQueue").click();
            if (moreMsgs) {
                document.getElementById("SendButton").click();
                // clicking "send" stops the timer!
            } else {
                console.log("Timer reached 0s, but moreMsgs == false");
                timer.stop();
            }
        }
    },

    start: function(seconds) {
        if (timer.setIntervalID !== 0) { timer.stop(); }
        timer.secondsLeft = seconds + 1;
        timer.tictoc();
        timer.setIntervalID = setInterval(timer.tictoc, 1000);
        timer.timerDIV.style.display = "initial";
    },

    stop: function() {
        clearInterval(timer.setIntervalID);
        timer.secondsLeft = -1;
        timer.timeLeftSPAN.innerHTML = timer.secondsLeft;
        timer.timerDIV.style.display = "none";
    },
};