package main

import (
	"time"

	"github.com/gorilla/websocket"
)

type expertiseType uint16
type playerIDType uint16
type eventIDType uint8
type connectionEventIDType uint8
type clickEvent uint

// Event ...
type Event struct {
	EventID    eventIDType
	SenderID   playerIDType
	ReceiverID playerIDType
	ReferredID playerIDType

	Question  expertiseType
	Expertise expertiseType

	Porto  int
	Reward int

	Clicked int

	FromAdmin bool
	Valid     bool
	Time      time.Time
}

// ConnectionEvent is used to inform the game-engine about connection events
// (new connections or connection-losses)
type ConnectionEvent struct {
	EventID  connectionEventIDType
	SenderID playerIDType
	ws       *websocket.Conn
}

// PlayerIDAdmin ...
const (
	PlayerIDAdmin playerIDType = 65534
	PlayerIDAll   playerIDType = 65535
)

// eventID meanings
const (
	EventMsgInquiry eventIDType = 1
	EventMsgReply   eventIDType = 2
	EventNoMoreMsgs eventIDType = 3

	EventNextRound eventIDType = 11
	EventNextStage eventIDType = 12

	EventStartTimer         eventIDType = 21
	EventStartQuestionnaire eventIDType = 22 // This could also be called "end game"

	EventClick eventIDType = 31

	EventAddPlayer                   eventIDType = 101
	EventShuffleContactLists         eventIDType = 102
	EventSuggestContact              eventIDType = 111
	EventRemoveAllContactSuggestions eventIDType = 112

	PlayerIDInvalid      eventIDType = 200
	PlayerIDConfirmation eventIDType = 201
)

// connectionEventID meanings
const (
	ConnEventNewAdmin     connectionEventIDType = 1
	ConnEventAdminLost    connectionEventIDType = 2
	ConnEventNewPlayer    connectionEventIDType = 11
	ConnEventPlayerLost   connectionEventIDType = 12
	ConnEventNewSpectator connectionEventIDType = 21
)

// Clicked meanings
const (
	ClickedSendButton     clickEvent = 1
	ClickedNextGameButton clickEvent = 2

	ClickedOther clickEvent = 10

	// contact labels 1001 to 1999 (eg 1005: contact 5 contact-label)
	// willSendButton 2001 to 2999 (eg 2005: contact 5 willSendButton)
)
