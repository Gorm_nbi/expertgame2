package main

import "os"
import "fmt"
import "time"

var toClickWriter = make(chan Event)

func clickWriter(dataDirName string) {
	clickEventsFile, err := os.Create(dataDirName + "/clickEvents.txt")
	if err != nil {
		fmt.Println("clickWriter error creating file:")
		panic(err)
	}
	fmt.Fprintln(clickEventsFile, "SenderID", "target", "hour", "minute", "second")
	for {
		event := <-toClickWriter
		fmt.Fprintln(clickEventsFile, event.SenderID, event.Clicked, time.Now().Format("15 04 05.000"))
	}
}
