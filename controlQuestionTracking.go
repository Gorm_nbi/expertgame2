package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/websocket"
)

type controlQuestionAnswer struct {
	ws     *websocket.Conn
	answer string
}

var toControlQuestionsWriter = make(chan controlQuestionAnswer)

func controlQuestionWriter(dataDirName string) {
	// create data-file
	controlQuestionsFile, err := os.Create(dataDirName + "/controlQuestionAnswers.txt")
	if err != nil {
		fmt.Println("clickWriter error creating file:")
		panic(err)
	}
	// handle input
	for {
		answers := <-toControlQuestionsWriter
		_, err := fmt.Fprintln(controlQuestionsFile, answers.answer)
		if err != nil {
			fmt.Println("Error printing questionnaire answer to file:")
			fmt.Println(err)
			answers.ws.WriteJSON("fail")
			return
		}
		answers.ws.WriteJSON("success")
	}
}

func wsControlQuestions(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	quickHandleError(err, "error upgrading control question connection")

	for {
		// read answers
		var answers map[string]string
		if err := ws.ReadJSON(&answers); err != nil {
			ws.Close()
			return
		}

		// extract player-id
		playerID, ok := answers["playerID"]
		if !ok || playerID == "" {
			playerID = fmt.Sprint(ws.RemoteAddr())
		}
		delete(answers, "playerID")

		answers["time"] = time.Now().Format("15.04.05")

		answerString, err := json.Marshal(answers)
		quickHandleError(err, "Error Marshal'ing control question answers:\n")

		toControlQuestionsWriter <- controlQuestionAnswer{
			ws:     ws,
			answer: "\"" + playerID + "\":" + string(answerString),
		}
	}
}

func quickHandleError(err error, context string) {
	if err != nil {
		fmt.Println(context)
		fmt.Println(err)
	}
}
