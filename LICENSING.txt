Copyright 2017 Gorm Gruner Jensen

"The expertgame2" is intended for reasearch.

Most of the code in this repossitory is all inspired by, or directly copied from: 

https://bitbucket.org/MaVo159/expertgame-server/src  | Copyright 2014-2016 Florian Uekermann


All files in this repository are licensed under GPLv3 with the following exceptions:

questionnaire.html:
 - This questionnaire contain a subset of the 60 question version of the HEXACO personality test:
   Ashton, M. C., & Lee, K. (2009). The HEXACO-60: A short measure of the major dimensions of personality. Journal of Personality Assessment, 91, 340-345.
 - Theses questions are available for free download on at http://hexaco.org/hexaco-inventory, but may only be used for non-profit research purposes.

Electronic_Chime-KevanGC-495939803.mp3
 - Published under Public Domain license at: http://soundbible.com/1598-Electronic-Chime.html

A copy of the GPLv3 license can be found in the following file:
gpl-3.0.txt

